import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { EquiposAddComponent } from './Equipos/add-equipos/equipos-add.component';
import { EquiposEditComponent } from './Equipos/edit-equipos/equipos-edit.component';
import { EquiposGetComponent } from './Equipos/get-equipos/equipos-get.component';

import { ToastrModule } from 'ng6-toastr-notifications';
import { SidebarjsModule } from 'ng-sidebarjs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EquiposService } from './services/equipos.service';
import { QRCodeModule } from 'angular2-qrcode';


@NgModule({
  declarations: [
     HomeComponent,
    AppComponent,
EquiposAddComponent,
EquiposGetComponent,
EquiposEditComponent

  ],
  imports: [
    NgbModule,
    SidebarjsModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    ToastrModule.forRoot(),
    QRCodeModule
  ],
  providers: [
    EquiposService

    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
