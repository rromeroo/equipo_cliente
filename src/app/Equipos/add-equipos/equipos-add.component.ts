import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { EquiposService } from '../../services/equipos.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-equipos-add',
  templateUrl: './equipos-add.component.html',
  styleUrls: ['./equipos-add.component.css']
})
export class EquiposAddComponent implements OnInit {
  Codigo_QR: any = {};
      angForm: FormGroup;
      otro : String;
  constructor(

   private fb: FormBuilder,
   private Equipos_ser: EquiposService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       Placa_Inventario: ['', Validators.required ],
Nombre: ['', Validators.required ],
Descripcion: ['', Validators.required ],
Codigo_QR: ['', Validators.required ],
Ubicacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addEquipos(Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion ) {
   
    this.Equipos_ser.addEquipos(Placa_Inventario, Nombre, Descripcion, Codigo_QR, Ubicacion );
    this.angForm.reset();
    
    this.showSuccess();
  }

  ngOnInit() {
   
  }

}
